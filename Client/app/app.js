var americanCupApp = angular.module('americanCupApp', ['ngResource', 'ui.router', 'ui.bootstrap', 'ngRoute'])

    .config(['$stateProvider', '$urlRouterProvider',  function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider

            // Informe RAG
            .state('/', {
                url: '/',
                controller: 'home_ctrl',
                templateUrl: 'Client/app/templates/home.html'
            })

    }])